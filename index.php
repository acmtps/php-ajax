<html>
	<head>
		<title>Simple PHP and AJAX application</title>
		<!-- <link rel="stylesheet" href="style.css" type="text/css"> -->
	</head>
	<body>
		<!-- Customer creation Form -->
		<div>
			<form action="" method="post">
				<label for="">Name</label>
				<input type="text" name="name" id="name">
				<label for="">Email</label>
				<input type="email" name="name" id="email">
				<label for="">Mobile</label>
				<input type="text" name="mobile" id="mobile">
				<button type="submit" name="save" id="save">Save</button>
			</form>
		</div>
		<!-- Filter -->
		<!-- List of customers -->
	</body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
		$('#save').click(function(e){
			e.preventDefault();
			var name = $('#name').val();
			var email = $('#email').val();
			var mobile = $('#mobile').val();
			// console.log(name);
			// console.log(email);
			// console.log(mobile);
			$.ajax({
				url: 'save.php',
				type: 'post',
				data:{
					name: name,
					email: email,
					mobile: mobile,
				},
				success: function(result){
					console.log(result);
				},	
				error: function(result){
					alert('error bleh');
				}
			});
		})
	});
</script>